#Projet de calcul distribué

## Installation

Vous devez avoir un serveur web de disponible sur votre machine.
Téléchargez ou cloner le dépôt et placer les fichiers pour qu'ils soient accessible via votre serveur web mis en place.
Il faut ensuite donner l'accès en écriture à votre serveur. Placez vous dans le répertoire de l'interface. Le nom de l'utilisateur est différent suivant votre distribution : 

**Ubuntu**

```
#!bash

sudo chown www-data .
sudo chmod .
```