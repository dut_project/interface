<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Système de calcul distribué</title>
	</head>
	<body>
		<h1>Système de calcul distribué</h1>
		<p>
			Le système de calcul distribué permet à un chercheur de lancer une expérience qui nécessite d'être exécuter plusieurs fois avec des paramètres différents. Le système se chargera ensuite de distribué le calcul à différentes machines inutilisées de l'IUT. <br />
			Pour cela vous devez envoyer votre exécutable compilé pour un linux 64 bits sans dépendance. Vous devez aussi spécifié le paramètre à faire varier :
		</p>
		<p>
			Attention, le système gère une seule expérience à la fois pour le moment. Enregistrer votre expérience maintenant supprimera l'expérience en cours. Veuillez vous assurer qu'elle est terminée.
		</p>
		<?php
			if (isset($_POST['minimum']) and isset($_POST['maximum']) and isset($_FILES['file']))	// Si le formulaire a été validé
			{
				if (empty($_POST['minimum']) == false and empty($_POST['maximum']) == false)	// Vérification des paramètres
				{
					if ($_FILES['file']['error'])   // Vérification du fichier
					{
						switch ($_FILES['file']['error'])	// Vérification de la cause de l'erreur
						{
							case 1: // UPLOAD_ERR_INI_SIZE
								echo "Le fichier dépasse la limite autorisée par le serveur (fichier php.ini).";
								break;
							case 2: // UPLOAD_ERR_FORM_SIZE
								echo "Le fichier dépasse la limite autorisée dans le formulaire HTML.";
								break;
							case 3: // UPLOAD_ERR_PARTIAL
								echo "L'envoi du fichier a été interrompu pendant le transfert.";
								break;
							case 4: // UPLOAD_ERR_NO_FILE
								echo "Il n'y a pas de fichier.";
								break;
						}
					}
					else        // Le fichier est bien arrivé
					{
						// On enregistre l'exécutable de l'expérience
						$chemin_destination = getcwd().'/';
						move_uploaded_file($_FILES['file']['tmp_name'], $chemin_destination.'experience');	// On enregistre le fichier dans experience/experience
						// On enregistre les paramètres de l'expérience
						if ($file = fopen(getcwd().'/parametres.txt', 'w'))
						{
							fwrite($file, $_POST['minimum'] . "\n" . $_POST['maximum']);
							fclose($file);
							echo "Expérience enregistrée.";
						}
						else
							echo "Erreur lors de l'enregistrement des paramètres.";
					}
				}
				else        // Si les paramètres sont erronés
				{
					echo "Les paramètres que vous avez spécifié ne sont pas valide.";
				}
			}
			else // Si le formulaire n'est pas validé on l'affiche
			{
				?>
				<form action="index.php" method="POST" enctype="multipart/form-data">
					Exécutable : <input type="file" name="file"><br />
					Valeur minimum du paramètre : <input type="number" placeholder="1" name="minimum"><br />
					Valeur maximum du paramètre : <input type="number" placeholder="100" name="maximum"><br />
					<input type="submit" value="Valider">
				</form>
				<?php
			}
		?>
	</body>
</html>
